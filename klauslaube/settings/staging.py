from settings.defaults import *

STATIC_ROOT = '/home/klauslaube_static'
MEDIA_ROOT = '/home/klauslaube_media'

DATABASES['default'] = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'klauslaube_db',
    'USER': 'root',
    'PASSWORD': '',
    'HOST': 'localhost',
    'PORT': '',
}
