from datetime import datetime
from base64 import b64encode
import os

from django.conf import settings


class MediaTree(object):
    def __init__(self, media_root=settings.MEDIA_ROOT,
            media_url=settings.MEDIA_URL):
        self.media_root = media_root + '/'
        self.media_url = media_url

        # Format media root and media url
        self.media_root = self._remove_last_slash(self.media_root)
        self.media_url = self._remove_last_slash(self.media_url)

        # Get paths and urls
        self.update_tree()

    def update_tree(self):
        self.tree = []
        self.paths = []
        self.urls = []
        self.files = []

        for path, dirs, files in os.walk(self.media_root):
            self.tree.append(path)
            self.paths.append(path)
            self.urls.append('%s/' % path.replace(self.media_root,
                self.media_url))

            for file_ in files:
                _path = os.path.join(path, file_)
                _url = _path.replace(self.media_root, self.media_url)
                self.tree.append(_path)
                self.urls.append(_url)
                self.files.append(self.file_infos(url=_url))

    def file_infos(self, url):
        splitted = os.path.split(url)
        path = os.path.join(self.media_root, '/'.join(url.split('/')[2:]))
        epoch = os.path.getmtime(path)

        return {
            'url': url,
            'path': path,
            'folder': '%s/' % splitted[0],
            'file': splitted[1],
            'date': datetime.fromtimestamp(epoch),
            'size': '%.2f KB' % (float(os.path.getsize(path)) / 1024),
            'code': b64encode(url),
        }

    def _remove_last_slash(self, path):
        if path.endswith('/'):
            return path[:-1]
