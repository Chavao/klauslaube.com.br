from django.test import TestCase
from portfolio.models import Customer, Job


class BasePortfolioModelTests(TestCase):
    fixtures = ['portfolio_testdata.json', ]

    def setUp(self):
        self.customer = Customer.objects.get(pk=1)
        self.job = Job.objects.get(pk=1)


class CustomerModelTests(BasePortfolioModelTests):
    def test_repr(self):
        self.assertEquals(str(self.customer), 'Oaken Shield')

    def test_bring_only_actives(self):
        self.customer.is_active = False
        self.customer.save()

        customers = Customer.actives.all()
        self.assertEquals(len(customers), 1)


class JobModelTests(BasePortfolioModelTests):
    def test_repr(self):
        self.assertEquals(str(self.job), 'Killing orcs')

    def test_get_absolute_url(self):
        self.assertEquals(self.job.get_absolute_url(),
            '/portfolio/killing-orcs/')

    def test_bring_only_actives(self):
        # Job status
        self.job.is_active = False
        self.job.save()

        jobs = Job.actives.all()
        self.assertEquals(len(jobs), 2)

        # Customer status
        customer = Customer.objects.get(pk=2)
        customer.is_active = False
        customer.save()

        jobs = Job.actives.all()
        self.assertEquals(len(jobs), 0)

    def test_convert_description_to_textile(self):
        self.job.details_source = "h1. Title!"
        self.job.save()

        self.assertEquals(self.job.details, "\t<h1>Title!</h1>")
