from django.contrib.sitemaps import Sitemap
from portfolio.models import Job


class PortfolioSitemap(Sitemap):
    changefreq = 'never'
    priority = 0.1

    def items(self):
        return Job.actives.all()
