from haystack import indexes
from portfolio.models import Job


class JobIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    pub_date = indexes.DateField(model_attr='creation_date')

    def get_model(self):
        """
        Return the Job model to indexes.
        """
        return Job

    def index_queryset(self):
        """
        Return only actives jobs from portfolio.
        """
        return Job.actives
