define("my/common", [
    "dojo/dom",
    "dojo/dom-class",
    "dojo/query",
    "dojox/fx/scroll"
], function(dom, domClass, query, fx) {
    
    return {
        init: function() {
            // When click an anchor, scroll smoothly to the node
            query("a[href^='#']").on("click", this.onAnchorClick);

            // When scroll, move the header together, but not in IE ;)
            if (domClass.contains(query("html")[0], "oldie")) {
                return;
            }
            
            var container = query(".container")[0];
            query(window).on("scroll", function(e) {
                if (this.scrollY > 360) {
                    domClass.add(container, "fixed-top");
                } else {
                    domClass.remove(container, "fixed-top");
                }
            });
        },
        
        // Events
        onAnchorClick: function(e) {
            e.preventDefault();
            var node = dom.byId(/#([\w_-]+)$/.exec(e.target.href)[1]);
            
            dojox.fx.smoothScroll({
                node: node,
                win: window
            }).play();
        }
    };

});
