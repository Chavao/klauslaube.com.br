from django.contrib.comments.moderation import CommentModerator
from django.core import management
from core.utils import slideshare_shortcode


class EntryModerator(CommentModerator):
    email_notification = True
    enable_field = 'enable_comments'


def replace_slideshare_shortcode(instance, **kwargs):
    """
    Replace the shortcode slideshare pattern to the respective
    markup.
    """
    instance.body = slideshare_shortcode(instance.body)
