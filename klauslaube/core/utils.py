import re

SLIDESHARE_PATTERN = r'\[slideshare\s+id=(?P<id>\d+)&[\w\s\&\=\_\-]+\]'


def slideshare_shortcode(content):
    """
    Interprets a text by applying the markup for displaying elements of
    slideshare.

    Usage::

        text = ('<p>See the presentation below:</p> '
            '[slideshare id=7916312&doc=font-face-verdana'
            '-110510185728-phpapp02]')
        response = slideshare_shortcode(text)
    """
    slideshare_frame = ('<iframe src="http://www.slideshare.net/slideshow'
        '/embed_code/%(id)s" width="590" height="481" frameborder="0" '
        'marginwidth="0" marginheight="0" scrolling="no"></iframe>')
    m = re.search(SLIDESHARE_PATTERN, content)

    while m:
        parms = m.groupdict()
        sub_pattern = SLIDESHARE_PATTERN.replace('(?P<id>\d+)', parms['id'])
        content = re.sub(sub_pattern, slideshare_frame % parms, content)

        m = re.search(SLIDESHARE_PATTERN, content)

    return content
