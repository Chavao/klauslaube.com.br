from sys import version_info as python_version
from django import VERSION as django_version
from django.conf import settings
from diario import VERSION as diario_version
from simple_contact import VERSION as simple_contact_version
from simple_links import VERSION as simple_links_version


def project_versions(request):
    """
    Returns the versions of Python packages used by this blog.
    """
    return {
        'PYTHON_VERSION': '%s.%s.%s' % python_version[:3],
        'DJANGO_VERSION': '%s.%s.%s' % django_version[:3],
		'DIARIO_VERSION': '%s.%s.%s' % diario_version[:3],
        'SIMPLE_CONTACT_VERSION': '%s.%s.%s' % simple_contact_version,
        'SIMPLE_LINKS_VERSION': '%s.%s.%s' % simple_links_version,
    }


def project_settings(request):
    """
    Returns values from settings used in templates.
    """
    return {
        'DEBUG': settings.DEBUG,
        'SITE_NAME': settings.SITE_NAME,
        'GOOGLE_ANALYTICS_KEY': settings.GOOGLE_ANALYTICS_KEY,
        'FB_ADMIN_ID': settings.FB_ADMIN_ID,
    }
